var controls = {
  keysets : [
    {"left": 37, "right": 39, "up": 38, "down": 40},
    {"left": 65, "right": 68, "up": 87, "down": 83},
    {"left": 71, "right": 74, "up": 89, "down": 72},
    {"left": 76, "right": 222, "up": 80, "down": 186},
  ],
  init : function() {
    this.keysDown = (this.keysDown || []);
    this.addEventListeners();
  },
  addEventListeners : function() {
    window.addEventListener('keydown', function (e) {
      controls.keysDown[e.keyCode] = (e.type == "keydown");
    });
    window.addEventListener('keyup', function (e) {
      controls.keysDown[e.keyCode] = (e.type == "keydown");
    });
  },
};
