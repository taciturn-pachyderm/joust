/*jslint white: true */
var hud = {
  update : function() {
    this.updateScores();
  },
  updateScores : function() {
    for (i = 0; i < players.players.length; i += 1) {
      players.players[i].scoreText.text = players.players[i].color + ": " + players.players[i].score;
      players.players[i].scoreText.update();
    }
  },
};
