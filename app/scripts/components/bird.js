var bird = {
  props : knobsAndLevers.birds,
  wrapAroundEdges : function(currentBird) {
    let rightEdge = knobsAndLevers.canvas.width;
    let leftEdge = 0;
    // wrap player to other side of screen when exiting canvas
    // TODO wrap player even if they are being pushed and not currently moving through controls
    if (currentBird.getLeft() > rightEdge) {
      currentBird.x = leftEdge - currentBird.width;
    };
    if (currentBird.getRight() < leftEdge) {
      currentBird.x = rightEdge;
    };
  },
  manageForces : function(currentBird) {
    currentBird.speedX *= this.props.friction;
    if (currentBird.y <= knobsAndLevers.canvas.height - this.props.height && currentBird.speedY < this.props.maxSpeed) {
      currentBird.speedY += this.props.gravity;
    };
  },
  checkPlatforms : function(currentBird) {
    currentBird.grounded = false;
    for (var i = 0; i < ledges.ledges.length; i++) {
      var dir = currentBird.getCollisionDirection(ledges.ledges[i]);
      if (dir === "left" || dir === "right") {
        currentBird.speedX = 0;
        break;
      } else if (dir === "bottom") {
        currentBird.grounded = true;
        break;
      } else if (dir === "top") {
        currentBird.speedY *= -1;
      };
    };
    if (currentBird.grounded) {
      currentBird.speedY = 0;
    };
  },
  checkImpact : function(attacker) {
    var killed = false;
    for (j = 0; j < players.players.length; j += 1) {
      var defender = players.players[j];
      // don't want it to attack itself
      if (attacker.id === defender.id) {
        continue;
      };
      var defenderHitDir = defender.getCollisionDirection(attacker);
      if (defenderHitDir === null) {
        continue;
      };
      if (defenderHitDir === "top") {
        attacker.score++;
        this.resetPosition(defender);
        return;
      } else {
        if (defender.directionX === this.props.directions.right && attacker.directionX === this.props.directions.right) {
          if (attacker.x < defender.x && attacker.y >= defender.y) {
            //attacker gets a point
            attacker.score++;
            this.resetPosition(defender);
            break;
          } else {
            //defender gets a point
            defender.score++;
            this.resetPosition(attacker);
            killed = true;
            break;
          };
        } else if (defender.directionX === this.props.directions.left && attacker.directionX === this.props.directions.left) {
          if (attacker.x > defender.x && attacker.y <= defender.y) {
            //attacker gets a point
            attacker.score++;
            this.resetPosition(defender);
            break;
          } else {
            //defender gets a point
            defender.score++;
            this.resetPosition(attacker);
            killed = true;
            break;
          };
        } else {
          // bounce them off each other if they hit face to face
          attacker.x += attacker.directionX * -5;
          defender.x += defender.directionX * -5;
        };
      };
      return killed;
    };
  },
  resetPosition : function(currentBird) {
    let canvas = knobsAndLevers.canvas;
    currentBird.x = getRandom(0, canvas.width - currentBird.width);
    currentBird.y = getRandom(0, canvas.height - currentBird.height);
  },
};
