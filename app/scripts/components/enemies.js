var enemies = {
  startX : (canvasWidth - bird.props.width) / 2,
  startY : canvasHeight / 2 - bird.props.height,
  init : function() {
    this.enemy = new component(bird.props.width, bird.props.height, "orange", this.startX, this.startY, "npc");
    this.enemy.directionX = -1;
    this.enemy.score = 0;
    this.enemy.startX = this.startX;
    this.enemy.startY = this.startY;
  },
  manage : function() {
    this.move();
    this.update();
  },
  update : function() {
    this.enemy.newPos();
    this.enemy.update();
  },
  resetPosition : function() {
    this.enemy.x = this.startX;
    this.enemy.y = this.startY;
  },
  move : function() {
    this.enemy.checkPlatforms(this.enemy);
    this.enemy.wrapAroundEdges(this.enemy);
    this.enemy.manageForces(this.enemy);
  },
};
