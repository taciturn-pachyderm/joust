var texts = {
  pausedMessage : undefined,
  init : function() {
    let canvas = knobsAndLevers.canvas;
    this.pausedMessage = new component("30px", "Consolas", "Black", canvas.width / 4, canvas.height / 4, "text");
    this.updatePause();
  },
  updatePause : function() {
    this.pausedMessage.text = "Paused: Spacebar to Continue";
    if (game.frameNo === 0) {
      this.pausedMessage.text = "Press Spacebar to Start";
    }
    this.pausedMessage.update();
  },
};
