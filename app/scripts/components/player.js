/*jslint white: true */
var players = {
  props : knobsAndLevers.players,
  players : [],
  manage : function() {
    if (game.frameNo == 1) {
      this.create();
    };
    this.move();
    this.update();
  },
  create : function() {
    while (this.players.length < this.props.amount) {
      let startX = this.props.startX;
      let startY = this.props.startY - 200 * (this.players.length);
      player = new component(bird.props.width, bird.props.height, getRandomColor(), startX, startY, "player");
      player.startX = startX;
      player.startY = startY;
      player.directionX = 1;
      player.score = 0;
      player.controls = controls.keysets[this.players.length];
      player.id = this.players.length + 1;
      player.scoreText = new component("30px", "Consolas", "black", knobsAndLevers.canvas.width / 10 * 2 * (this.players.length + 1), 40, "text");
      this.players.push(player);
    };
  },
  update : function() {
    for (i = 0; i < this.players.length; i += 1) {
      this.players[i].newPos();
      this.players[i].update();
    };
  },
  move : function() {
    for (i = 0; i < this.players.length; i += 1) {
      killed = bird.checkImpact(this.players[i]);
      if (killed) {
        continue;
      };
      bird.checkPlatforms(this.players[i]);
      bird.wrapAroundEdges(this.players[i]);
      if (!controls.keysDown) {
        return;
      };
      // left
      if (controls.keysDown[this.players[i].controls['left']]) {
        this.players[i].directionX = -1;
        // move it
        this.players[i].speedX = -bird.props.speed;
      };
      // right
      if (controls.keysDown[this.players[i].controls['right']]) {
        this.players[i].directionX = 1;
        // move it
        this.players[i].speedX = bird.props.speed;
      };
      // up
      if (controls.keysDown[this.players[i].controls['up']] && this.players[i].y > 0) {
        this.players[i].speedY = -bird.props.speed;
        this.players[i].grounded = false;
      };
      bird.manageForces(this.players[i]);
    };
  },
  clear : function() {
    this.players = [];
  },
};
